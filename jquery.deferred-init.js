/**
 * Implements the Deferred Initiation Pattern
 * to prevent multiple handler
 * execution
 */
;(function($){
	// Usage: 
	// $('.selector')
	//    .d()
	// 	  .on('click', handler)
	$.fn.d = function() {
		
		$.each(this, function(idx,ele){
			// Each element must have a delay handler
			if (!ele.__d_handles) 
				ele.__d_handles = {};
		});
		function deferred_handler(event, handler) {
			//console.log("deferred_handler", event);
			return function() {
				//console.log("deferred_handler", this);
				if (this.__d_handles[event]) {
					clearTimeout(this.__d_handles[event]);
				}
				this.__d_handles[event] = setTimeout(handler, 1000);
			};
		}

		// Proxy
		var proxied_on = this.on;
		this.on = function(event) {
			var handler = arguments[arguments.length-1];
			arguments[arguments.length-1] = deferred_handler(event, handler);
			//console.log('Proxied!');
			proxied_on.apply(this, arguments);
			return this;
		};
		// "this" is the jQuery wrapped object
		return this;
	}
})(jQuery);